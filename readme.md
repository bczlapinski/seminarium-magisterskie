###### Te repozytorium będzie zawierało pliki dotyczące pracy magisterskiej tworzonej podczas studiów na kierunku Informatyka na Uniwersytecie Gdańskim.
---

## AMP jako narzędzie przyśpieszanie stron internetowych z uwzględnieniem użyteczności
##### Słowa kluczowe: frontend, UX, usability, js, javascript, loading, preview, wydajnosc, uzytecznosc, UI, AMP

#

> *Hofstadter's Law: It always takes longer than you expect, even when you take into account Hofstadter's Law.*

>— Douglas Hofstadter, Gödel, Escher, Bach: An Eternal Golden Braid,  20th anniversary ed., 1999, p. 152

#

> The NET is a waste of time, and that's exactly what's right about it.

>— William Gibson Title of an article for New York Times Magazine (14 July 1996).

#

> Miernikiem wartości strony nie jest liczba osób, które ją odwiedziły, tylko liczba osób, które na nią powróciły.

>— Michael Dell

## Cel pracy magisterskie :
Praca magisterska to praca dyplomowa, która zobowiązuje do podjęcia tematów, które wymagają poznania i zrozumienia oraz wykorzystania literatury teoretycznej i naukowych metod do analizowania i oceny zebranych informacji. („Praca dyplomowa nauką i sztuką” autorstwa prof. Polańskiej). Praca magisterska musi również charakteryzować się wykorzystaniem metody naukowej, stąd nacisk na pracę badawczą. Za "Instrukcja przygotowania prac dyplomowych" (licencjackich i magisterskich) przygotowaną przez Wydział Ekonomiczny UG z kolei "praca magisterska z kolei jest dowodem na to, że jej autor potrafi w sposób innowacyjny rozwiązywać problemy w danej dziedzinie wiedzy i potrafi to opisać oraz udokumentować w formie opracowania nadającego się do szerszego wykorzystania.". W związku z powyższymi postanowiłem zając się w swojej pracy badaniem na temat wpływu formatu publikowania stron internetowych AMP na wydajność stron internetowych wraz z zachowaniem/ulepszeniem użyteczności. Wprowadzone przeze mnie pojęcia definiuje za pomocą istniejących źródeł, dzięki czemu moja praca mieści się w istniejących już ramach zagadnienia - poszerzając je za pomocą własnych badań (w przypadku tej pracy testów) i wyników pracy nad nimi. 
Powód wybrania tego tematu znajduje się we wstępie, w streszczeniu podaje mój postęp pracy nad zagadnieniem, w pliku aktualnie przeglądanym znajduje się również spis treści pracy. 

## Wstęp :
Korzystając z internetu naszą podstawową metoda od czasu jego powstania są strony internetowe. Jako student wiem, że kwestia stabilności połączenia jest najważniejszym aspektem wydajnej pracy z materiałami www. Często korzystam z internetu na uczelni, w drodze do pracy lub podczas podróży pociągiem. Uważam również, że choć web 2.0 istnieje od kilkunastu lat (za jedną z umownych dat uznaje się rozpowszechnienie mediów społecznościowych ok. 2006), to statyczne strony internetowe strony internetowe są głównymi kontenerami dla zawartości merytorycznej sieci. I mimo, że Single Page Application zastępują tradycyjne statyczne strony, to właśnie tworzona i umieszczana na stronach zawartość pozwala istnieć mediom społecznościowym. Nawet takie strony są mimo rozwoju metod tworzenia stron (nowa klasyfikacja HTML itp.) wrażliwe na możliwości łącza przesyłowego.

Dziś znaczący procent użytkowników dzięki popularności mobilnych przeglądarek korzysta z internetu podczas podróży czy po prostu poza domem. Stawia to przed front endem nowe problemy - strony kolokwialnie mówiąc urosły, przez nadmierne stosowanie js i ozdobników. Dlatego w swojej pracy postanowiłem zaimplementować istniejące rozwiązania zagadnienia wydajności stron internetowych (w szczególności mobilnych, z tego względu nacisk na responsywność i tym samym użyteczność), a tym samym problemu ograniczonej wydajności łącz. 

Modularna budowa aplikacji sieciowych pozwala na łatwe ich testowanie ( ze względu na możliwość pracy nad poszczególnymi modułami i tym samym przeprowadzaniu testów wykorzystujących to do podzielenia zagadnienia i wyszczególnienia miejsc występowania błędów) oraz ich modyfikowanie. Zachowana jest przy tym skala projektu i jego użyteczność.  Skuteczny system musi być zdolny do obsługi znacznej ilości użytkowników – zarówno pod względem przetwarzania dużej ilości danych, jak i przebiegu doświadczenia podczas używania strony (przebiegu od uzyskania dostępu przez użytkownika do opuszczenia serwisu) - testy powinny wykazać, że jedne rozwiązania nie blokują innych, to znaczy system nie ma przestojów spowodowanych trudnością użytkowaniu, jak również ograniczeniami oprogramowania. 

Chciałbym za pomocą testów wydajności (szybkość renderowania strony u użytkownika końcowego) udowodnić, że technologia AMP znacząco poprawia wyniki czasowe pomiędzy połączeniem z serwerem, a pełnym otrzymaniem strony u użytkownika.
Planuje również pokazać miejsce tej technologii w istniejących już metodologiach, jak również zagrożenia i inne zalety stosowania tej stworzonej przez Google metodologii. W tym celu na 5 przykładach, o odmiennej charakterystyce opisanej dalej w pracy, zastosuje stworzony przeze mnie plan testów i zinterpretuje wyniki korzystając z metod statystycznych, na ile jest to możliwe dla tej wielkości zbioru danych. Użyte przeze mnie statystyki i opinie potwierdzę za pomocą dołączonych przypisów, wyjaśnionych w rozdziale końcowym. 

Pojęciami ważnymi dla tematyki mojej pracy są  więc wydajność i użyteczność.
Aplikacje sieciowe używane są przez użytkowników nie będących częścią procesu tworzenia samej aplikacji, tym samym ich doświadczenie użytkownika jest inne niż twórcy oprogramowania (ang.User Experience). Zmieniają się też tym samym kryteria wyznaczające wzorce użyteczności, nacisk położony jest na szybkie działanie systemu i łatwość jego użytkowania, tak jak wspomniane zostało wcześniej.

Usability,  nazywane w języku polskim bliskoznacznym słowem użyteczność, definiujemy jako własność produktu określającą jakość użytkową. Jest to pojęcie powiązane w tym względzie z zasadami wzornictwa przemysłowego (ang. industrial design).

Profesjonalne doświadczenie użytkownika (UX) jest definiowane jako wynik stabilnej i wydajnej pracy back-endu aplikacji oraz użyteczności stworzonej przez front-end developerów. Testowanie staje się częścią tworzenia spełniającego wymagania projektu front-endu - historie użytkownika (ang. User Story) określają wymagania formalne wobec testów, jak i tworzonej funkcjonalności.

Wydajność to współczynnik oznaczający szybkość renderowania strony wraz z pełną użytecznością u klienta. Na wydajność ma wpływ użyty serwer, jak i parametry samego użytkownika końcowego. Pod uwagę będą więc brane wyniki na określonej specyfikacji wraz z powtórzeniami, częściowo ograniczając i pozwalając ujednolicić wyniki testów - otrzymując stosunki czasu do użytej technologii przy traktowaniu specyfikacji jako zmiennej niezależnej. 

Podsumowując praca dotyczy implementacji AMP, jej skutku i wpływu na wydajność stron na przykładach w utworzonych w kilku technologiach. 


## Streszczenie:
W swojej pracy zająłem się zagadnieniem wydajności i jej powiązania z użytecznością na przykładzie użycia biblioteki AMP. Stworzyłem lub wybrałem przykłady według określonych kryteriów stron internetowych na których sprawdzana jest teza - AMP jako narzędzie zwiększania wydajności. Dotychczasowa praca dotyczyła również wprowadzenia środowiska pracy i metodologii zastosowanych, wyprowadzenia skutecznych metod porównawczych i badawczych oraz zinterpretowania wyników analizy. Do interpretowania wyników analizy zastosowany zostanie język R. Ze względu na konieczność analizy i interpretacji danych liczbowych dotyczących wydajności stron internetowych konieczne było ujednolicenie i określenie wpływu używanego środowiska na wyniki. Głównym środowiskiem pracy jest system Windows. Pomocniczym środowiskiem pracy jest Cloud9 pod kontrolą systemu Linux, dystrybucji Ubuntu.

Pracą związaną z seminarium, mająca na celu udowodnić dodatkowo potrzebę pracy nad zagadnieniem, była praca nad wykorzystaniem istniejących już danych ze Stackoverflow. Do tego celu przetworzony zostanie istniejący skrypt, który pobierze dane do przetworzenia dotyczące problemów pracy jako frontend developer. 

Stworzone zostało kilka *borderplate* stron (potocznie w języku polskim ramy) 5 przykładowych stron - są to strony open source lub stworzone na potrzeby pracy. Część stron korzysta z proponowanej technologii AMP, część jest stronami internetowymi stworzonymi bez tej technologii, celem porównania. Dla uniknięcia powiązania konkluzji z własną zdolnością interpretacji rozwiązania (tzn. pisanie pod testy) zastosowane zostały również strony stworzone przez zew. deweloperów - oficjalne przykłady użycia danej technologii lub strony *opensource* dostępne na licencji CC. 

##### Spis treści (w nawiasach podrozdzialy)
* Wstęp
* Streszczenie
* Zarys zagadnienia 
* [Ważność wydajności]
* [Wpływ użyteczności]
* [Dotychczasowe rozwiązania]
* Proponowane rozwiązania 
* Implementacja przykładów stron
* Użyte narzędzia 
- [Narzędzia przyśpieszające strony internetowe]
- [Narzędzia testujące wydajność stron internetowych]
- [Pozostałe użyte narzędzia]
* Wyniki badań i testów 
* Praca nad implementacja przykładów
- [Problemy w implementacji]
- [Problemy w zgodności]
- [Problemy z bezpieczeństwem danych]
* Przyszłość rozwiązań
* Konkluzje
* Bibliografia
* Przypisy

##### Dodatkowe badania - ankieta
Automatyczna ankieta na podstawie istniejącego skryptu - uzyskiwanie danych i dalsze ich obrobienie - automatycznie zbierane po tagach dane ze Stackoverflow dotyczące charakterystyki pytań dotyczących trudności pracy we Frontendzie osadzonych w tematyce zagadnienia problemu.

##### Zagadnienia do prezentacji
Cel pracy, zagadnienie pracy, terminologia, metody badawcze, wyniki badań wstępnych, dalsze prace (zmiana konceptu? kontynuacja zagadnienia? zmiana metodologi badawczej? rozwinięcie obecnej?) 

##### Prezentacja - zagadnienie
Cel pracy (testowanie możliwości usprawnienia wydajności stron internetowych), zagadnienie pracy (zastosowane technologie, wcześniejsze prace innych nad tematem, sposoby osiągania wyników przydatnych w pracy), terminologia, metody tworzenia pracy (użyte narzędzie, strony internetowe, bibliografia, opinie stron zewnętrznych), praca nad uniknięciem niepotrzebnych zagadnień utrudniających uzyskanie miarodajnych wyników (np. metody badawcze odrzucone)

##### Prezentacja - prace nad zagadnieniem
W dołączonym pliku

##### Prezentacja - postęp prac

## Cele krótkoterminowe
* Dodać tabele
* Dodać przypisy 
* Poprawić stylistycznie, gramatycznie i strukturalnie
* Dodać wyniki badań
* Przenieść archiwalne teksty do pliku przeznaczonego 
* Sprawdzić zasady stosowania języka specjalistycznego w pracach magisterskich
* Dodać podstawowe wersje pozostałych 3 stron
* Dodać opisy technologii stowarzyszonych i pobocznych

## Użyte narzędzia i zasoby
* [Open source web desing](http://www.oswd.org/)
* [AMP - dokumentacja](https://www.ampproject.org/learn/about-amp/)
* [Cloud9][https://c9.io/]

## Bibliografia:
* Tajniki języka JavaScript. Asynchroniczność i wydajność. Autor: Kyle Simpson, ISBN 9788328321724 
* Building the Realtime User Experience. Autor: Ted Roden, ISBN 9780596806156
* [Continuous Delivery and Rolling Upgrades](http://docs.ansible.com/ansible/guide_rolling_upgrade.html)
* [See Python, See Python Go, Go Python Go](https://blog.heroku.com/archives/2016/6/2/see_python_see_python_go_go_python_go?c=7013A000000mLBxQAM&utm_campaign=Newsletter_June_2016&utm_medium=email&utm_source=nurture&utm_content=blog&utm_term=see-python-go) 
* Web development. Receptury nowej generacji. Autorzy: Brian P. Hogan, Chris Warren, Mike Weber, Chris Johnson, Aaron Godin 
* Projektowanie interfejsów. Autor: Jenifer Tidwell, ISBN 9788324637416
* Redesign the Web. The Smashing book #3 Autor: Smashing Magazine, opracowanie, ISBN 978-3-943075-40-3 (wersja drukowana)
* Projektowanie witryn internetowych. User eXperience. Autor: Smashing Magazine, opracowanie, ISBN 9788324663491
* Kuloodporne strony internetowe. Jak poprawić elastyczność z wykorzystaniem XHTML-a i CSS. Autor: Dan Cederholm, ISBN 9788324603169
* UX i analiza ruchu w sieci. Praktyczny poradnik. Autor: Michael Beasley, ISBN 9788324690190
* [Standard IEEE pisania prac naukowych](http://www.ieee.org/publications_standards/publications/authors/authors_journals.html)
* [Rozwój AMP - konferencja Google I/O - podsumowanie autorstwa Dobreprogramy.pl](https://www.dobreprogramy.pl/AMP-turbodoladowane-mobilne-strony-w-Google-dwukrotnie-szybsze,News,81150.html)

---

Autor @bczlapinski